package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import javax.annotation.processing.SupportedSourceVersion;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	private int filas;
	
	private int columnas;
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	private boolean contraPc;
	
	public Interfaz()
	{
		imprimirMenu();

		sc= new Scanner (System.in);
		while (true)
		{
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					
					contraPc = false;
					empezarJuego();
				}
				else if(opt==2)
				{
					contraPc=true;
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");	
		
		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		int x = 0;
		int y = 0;
		ArrayList<Jugador> jugadores = new ArrayList<>();
		
		Scanner sc2 = new Scanner(System.in);
		try{
			System.out.println("cantidad de jugadores");
			int jugador = Integer.parseInt(sc.next());
			
			for(int i =0; i<jugador;i++){
				
			System.out.println("Nombre jugador"+(i+1));
			String nombre = sc.next();
			
			System.out.println("Simbolo jugador"+(i+1));
			String simbolo = sc.next();
			
			Jugador jugadorx = new Jugador(nombre, simbolo);
			jugadores.add(jugadorx);
			

				
			}
			
			
			System.out.println("cantidad de filas");
			filas = Integer.parseInt(sc.next());
			
			System.out.println("Cantidad de columnas");
			columnas = Integer.parseInt(sc.next());
			if(filas<4||columnas<4){
				throw new Exception("Las columnas y filas deben ser mayor a 4");
			}
			juego = new LineaCuatro(jugadores, filas, columnas);
			
			imprimirTablero();

			juego();
				
				

		}
			catch (Exception e) {
				
				 System.out.println(e.getMessage());
				
			}
		
		}
		
       //TODO
	
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		
	System.out.println("digite su coordenada separada por una coma "+juego.darAtacante());
	String c = sc.next();
	String coordenada[] = c.split(",");

	String x = coordenada[0];
	String y = coordenada[1];
	
	System.out.println(x+y);
	
	juego.registrarJugada(Integer.parseInt(x),Integer.parseInt(y)	);
	imprimirTablero();
	juego.terminar(Integer.parseInt(x), Integer.parseInt(y));
	if(contraPc==true){
		
	}
	if(contraPc==false){
		
	}
		
		
		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList<Jugador> jugadores = new ArrayList<>();//TODO
		try {
			System.out.println("computador= jugador 1");
			System.out.println("simbolo del computador = PC");
			
			System.out.println("Nombre jugador");
			String nombre = sc.next();
			
			System.out.println("Simbolo jugador");
			String simbolo = sc.next();
			
			System.out.println("cantidad de filas");
			filas = Integer.parseInt(sc.next());
			
			System.out.println("Cantidad de columnas");
			columnas = Integer.parseInt(sc.next());
			
			if(filas<4||columnas<4){
				
				throw new Exception("Las columnas y filas deben ser mayor a 4");
			}

			Jugador pc = new Jugador("PC", "PC");
			jugadores.add(pc);
			Jugador jugador = new Jugador(nombre, simbolo);
			jugadores.add(jugador);

			juego = new LineaCuatro(jugadores, filas, columnas);

			imprimirTablero();
			juegoMaquina();
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		juego.registrarJugadaAleatoria();
		
		System.out.println("////////////////////////");
		imprimirTablero();
		
		
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		
		String x[][]=juego.darTablero();

		
		for(int i = 0; i<columnas;i++ 	){

			
			
			for ( int j =0; j<filas;j++){
				
				
				
				if(j<filas-1){
				System.out.print(x[i][j]);}
				else{
					System.out.println(x[i][j]);
				}
			}
		}
				
				
				
			
	}
}
