package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	private int fila;
	
	private int columna;
	
	private Random rand;
	
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		fila = pFil;
		columna = pCol;
		tablero = new String[fila][columna];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="__|";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		
		
			
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		
		ArrayList<String> espaciosLibres = new ArrayList<>();
		
		for (int i = columna-1; i>-1;i--){
		
			
			for(int j =0;j<fila;j++){
				
				
				
				if (tablero[i][j].equals("__|")){
					
					
					espaciosLibres.add(i+","+j);
					
				}
				
				
			}
if(!espaciosLibres.isEmpty()&&turno==0){
				
				Random r = new Random();
				int Low = 0;
				int High = espaciosLibres.size()-1;
				int Result = r.nextInt(High-Low) + Low;
				
				String[] coordenadas = espaciosLibres.get(Result).split(",");
				int x = Integer.parseInt(coordenadas[0]);
				int y = Integer.parseInt(coordenadas[1]);
				
				tablero[x][y]="_pc|";
				
				invertirTurnoAndAtacante();
				
				break;
			}
		
		}
		
		
		
		//TODO
	}
	
	/**
	 * Registra una jugada en el tablero
	 * 
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int x, int y) {
		boolean registro = false;
		try {
			ArrayList<String> espaciosLibres = new ArrayList<>();

			if (!tablero[x][y].equals("__|")) {

				throw new Exception("ya hay un simbolo en este espacio");
			}
			
			for (int i = columna-1; i>-1;i--) {

				for (int j = 0; j < fila; j++) {

					if (tablero[i][j].equals("__|")) {
						espaciosLibres.add(i + "," + j);
						if (x == i && !espaciosLibres.isEmpty()) {

							Jugador jugador = jugadores.get(turno);
							String sim = jugador.darSimbolo();
							tablero[x][y] = "_"+sim+"|";

							invertirTurnoAndAtacante();

							registro = true;
						}

					}

				}
				
				
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return registro;

	}
	
	public void invertirTurnoAndAtacante(){
		
		if (turno == 0) {
			turno = 1;
			atacante = jugadores.get(turno).darNombre();
		}
		if (turno == 1) {
			turno = 0;
			atacante = jugadores.get(turno).darNombre();
		}
		
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		
		
		//TODO
		return true;
	}



}
